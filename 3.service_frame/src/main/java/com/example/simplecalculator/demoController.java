package com.example.simplecalculator;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicLong;

@RestController
public class demoController {

    @RequestMapping("/add")
    public double add(@RequestParam(value = "a") double a, @RequestParam(value = "b")double b)
    {

        return new SimpleCalculator(a+"+"+b).calculating();
    }

    @RequestMapping("/sub")
    public double sub(@RequestParam(value = "a") double a,@RequestParam(value = "b")double b)
    {
        return new SimpleCalculator(a+"-"+b).calculating();
    }

    @RequestMapping("/multiply")
    public double multiply(@RequestParam(value = "a") double a,@RequestParam(value = "b")double b)
    {
        return new SimpleCalculator(a+"*"+b).calculating();
    }

    @RequestMapping("/division")
    public double division(@RequestParam(value = "a") double a,@RequestParam(value = "b")double b)
    {
        return new SimpleCalculator(a+"/"+b).calculating();
    }
}

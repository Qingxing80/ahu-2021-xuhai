import java.util.Scanner;

/**
 * @title: Main
 * @author: xuhai
 * @date 2021/10/7 23:01
 */

//实际使用Cal
public class Main {
    public static void main(String[] args) {
        String expression;
        Scanner in = new Scanner(System.in);
        System.out.println("Please enter the expression you want to calculate:");
        expression = in.next();

        CalFactory cf = new CalFactory();
        Cal cal = cf.getCal(expression);
        cal.calculating();
    }
}

import java.util.*;

/**
    * @title: SimpleCalculator
    * @author: xuhai
    * @date 2021/10/7 23:01
    */

/**
这个简单计算器类的核心原理是：中缀表达式转后缀表达式，后缀表达式求值。使用栈实现
 */

//实现Cal接口的实体类(产品类)
public class SimpleCalculator implements Cal{

    String expression;

    //构造器
    public SimpleCalculator(String expression) {
        this.expression = expression;
    }

    //用户使用的计算器
    @Override
    public void calculating()
    {
        List<String> midExp = midExpression(this.expression);
        List<String> sufExp = sufExpression(midExp);
        System.out.println(calculate(sufExp));
    }

    //计算中缀表达式的的值
    @Override
    public double calculate(List<String> sufExp)
    {
        double result = 0;
        double a,b;
        //遗留类Stack已不推荐使用，故使用队列Deque来实现栈
        Deque<String> stack = new ArrayDeque<>();

        for(String item:sufExp)
        {
            //使用正则表达式匹配数字
            if(item.matches("-?[0-9]+\\.?[0-9]*"))
            {
                stack.push(item);
            }
            else
            {
                //如果不是运算符，则将栈顶的两个数取出进行运算
                b = Double.parseDouble(stack.pop());
                a = Double.parseDouble(stack.pop());
                switch (item)
                {
                    case "+":
                        result = a + b;
                        break;
                    case "-":
                        result = a - b;
                        break;
                    case "*":
                        result = a * b;
                        break;
                    case "/":
                        result = a / b;
                        break;
                }
                stack.push(""+result);
            }
        }
        //栈中最后剩下的数，即为该后缀表达式的值
        return Double.parseDouble(stack.pop());
    }


    //将存储在List集合的中缀表达式转换为后缀表达式
    @Override
    public List<String> sufExpression(List<String> list)
    {
        Deque<String> s1 = new ArrayDeque<String>();//栈，存放符号
        List<String> s2 = new ArrayList<>();//存放后缀表达式

        for(String item:list)
        {
            if(item.matches("-?[0-9]+\\.?[0-9]*"))
            {
                s2.add(item);
            }else if(item.equals("("))
            {
                s1.add(item);
            }else if(item.equals(")"))
            {
                while(!s1.peek().equals("("))
                {
                    s2.add(s1.pop());
                }
                s1.pop();
            }else
            {
                //使用辅助类来比较运算符的优先级
                while(s1.size() != 0 && Operation_.getValue(item) <= Operation_.getValue(s1.peek()))
                    s2.add(s1.pop());

                s1.push(item);
            }
        }

        while(s1.size() != 0)  //将s1中剩余的运算符依次弹出并加入s2
        {
            s2.add(s1.pop());
        }
        return s2;
    }


    //将一个中缀表达式字符串，转换为一个存储String值的List集合
    @Override
    public List<String> midExpression(String s)
    {
        List<String> list = new ArrayList<>();
        int i = 0;
        boolean isMinus;
        String str;
        char c;

        do
        {
            isMinus = false;
            if((c = s.charAt(i)) == '-')
            {
                if(i == 0 || s.charAt(i-1) == '(')
                    isMinus = true;
            }
            if(((c = s.charAt(i)) < 48 || (c = s.charAt(i)) > 57) && !isMinus)
            {
                list.add("" + c);
                i++;
            }else
            {
                str = "";
                do
                {
                    str += c;
                    i++;
                }
                while(i < s.length() && (((c = s.charAt(i)) >= 48 && (c = s.charAt(i)) <= 57) || (c = s.charAt(i)) == '.'));
                //表达式中的数字可能不止一位，可能带有小数点
                list.add(str);
            }
        }while(i < s.length());
        return list;
    }



}



//辅助类。为四种运算设置优先级，以便于中缀转后缀时的比较
class Operation_{
    final static private int ADD = 1;
    final static private int SUB = 1;
    final static private int MUL = 2;
    final static private int DIV = 2;


    public static int getValue(String op)
    {
        int res = 0;
        switch(op)
        {
            case "+":
                res = ADD;
                break;
            case "-":
                res = SUB;
                break;
            case "*":
                res = MUL;
                break;
            case "/":
                res = DIV;
                break;
        }

        return res;
    }
}
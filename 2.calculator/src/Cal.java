import java.util.List;

/**
 * @title: Cal
 * @author: xuhai
 * @date 2021/10/7 23:01
 */

//一个顶层计算器接口
public interface Cal {

    //用户使用的计算器
    void calculating();

    //计算中缀表达式的的值
    double calculate(List<String> sufExp);

    //将存储在List集合的中缀表达式转换为后缀表达式
    List<String> sufExpression(List<String> list);

    //将一个中缀表达式字符串，转换为一个存储String值的List集合
    List<String> midExpression(String s);
}

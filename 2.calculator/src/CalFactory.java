/**
 * @title: CalFactory
 * @author: xuhai
 * @date 2021/10/7 23:01
 */

//顶层计算器的工厂类
public class CalFactory {

    //根据实际情况选择Cal接口的具体实现类(产品类)，此次只有一个选择(SimpleCalculator)
    public Cal getCal(String expression)
    {
        return  new SimpleCalculator(expression);
    }
}

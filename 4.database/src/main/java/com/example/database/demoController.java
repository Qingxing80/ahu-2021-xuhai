package com.example.database;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class demoController {
    @Autowired
    JdbcTemplate jdbcTemplate;

    @RequestMapping("/add")
    public double add(@RequestParam(value = "a") double a, @RequestParam(value = "b")double b)
    {
        String sql = new BuildSql(a,"+",b,a+b).build();
        jdbcTemplate.update(sql);
        return new SimpleCalculator(a+"+"+b).calculating();
    }

    @RequestMapping("/sub")
    public double sub(@RequestParam(value = "a") double a,@RequestParam(value = "b")double b)
    {
        String sql = new BuildSql(a,"-",b,a-b).build();
        jdbcTemplate.update(sql);
        return new SimpleCalculator(a+"-"+b).calculating();
    }

    @RequestMapping("/multiply")
    public double multiply(@RequestParam(value = "a") double a,@RequestParam(value = "b")double b)
    {
        String sql = new BuildSql(a,"*",b,a*b).build();
        jdbcTemplate.update(sql);
        return new SimpleCalculator(a+"*"+b).calculating();
    }

    @RequestMapping("/division")
    public double division(@RequestParam(value = "a") double a,@RequestParam(value = "b")double b)
    {
        String sql = new BuildSql(a,"/",b,a/b).build();
        jdbcTemplate.update(sql);
        return new SimpleCalculator(a+"/"+b).calculating();
    }
}

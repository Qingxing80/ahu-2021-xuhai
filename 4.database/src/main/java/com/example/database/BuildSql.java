package com.example.database;

public class BuildSql {
    String op;
    double a;
    double b;
    double res;
    public BuildSql(double a,String op,double b,double res){
        this.op = op;
        this.a = a;
        this.b = b;
        this.res = res;
    }
    public String build(){
        StringBuilder sb = new StringBuilder("insert into ");
        sb.append("ahu_2021_test.calc_record(operand1,oprator,operand2,result) ");
        sb.append("value(");
        sb.append(a+",");
        sb.append("\'"+op+"\'"+",");
        sb.append(b+",");
        sb.append(res);
        sb.append(");");
        return sb.toString();
    }
}

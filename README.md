# AHU-2021-xuhai



## 一.git

1. 在`gitlab`创建个人的公共项目`ahu-2021-xuhai`.
2. 在电脑上安装`git`.创建SSH私钥和公钥.
3. 将SSH公钥粘贴到`gitlab`的`SSH Keys`中
4. 将`gtilab`上的远程库克隆到本地库
5. 在本地库中进行代码编写、更新
6. 工作完成后推送到`gitlab`远程库



## 二.编程

实现一个简单的计算器

### 1.介绍

基本原理：读入用户输入的中缀表达式，将中缀表达式转换为后缀表达式，对后缀表达式进行求值，输出计算结果。

所以，本项目实现的计算器，不仅可以计算类似于`4*6`的两个一位数字的四则运算，也可以计算类似与`12*23`、`-16*9`等包含多位数字、负数的四则运算，还可以计算类似于`(-6+9)/2`、`12/(4+6)-(-9)`等相对复杂的四则计算式。

### 2.实现(`SimpleCalculator.java`)

#### (1)中缀表达式

```java
public List<String> midExpression(String s){...}
```

该方法将用户输入的中缀表达式(`String`)，转换存储到一个集合中(`List<String>`)

#### (2)后缀表达式

```java
public List<String> sufExpression(List<String> list){...}
```

该方法将中缀表达式转换为后缀表达式。后缀表达式也存放在`List<String>`中。

规则：从左到右遍历中缀表达式的每个数字和符号，若是数字就输出，即成为后缀表达式的一部分；若是符号，则判断其与栈顶符号的优先级，是右括号或者优先级不大于栈顶符号则栈顶元素依次出栈并输出，并将当前符号入栈，一直到最终输出后缀表达式为止。

#### (3)后缀表达式求值

```java
public double calculate(List<String> sufExp){...}
```

该方法用于计算后缀表达式的值。

规则：从左到右遍历表达式的每个数字和符号，遇到是数字就进栈，遇到是符号就讲处于栈顶的两个数字出栈，进行运算，运算结果进栈，一直到最后获得结果。

#### (4)判断符号优先级

```java
class Operation_{
    ...
    public static int getValue(String op){....}
}
```

为了判断运算符号的优先级，编写了一个辅助类，该类中的方法用于得到某个运算符的优先级。



### 3.设计模式-工厂方法

本项目中应用了工厂方法的设计模式。

1. 顶层计算器接口`Cal.java`

   ```java
   public interface Cal{...}
   ```

2. 计算器接口的实现类(产品类)`SimpleCalculator.java`

   在工厂方法设计模式中，通常同一个接口会有若干个具体的实现类，以便于在不同应用场景下的不同选择。本例中只编写了一个实现。

   ```java
   public class SimpleCalculator implements Cal{...}
   ```

3. 创建工厂`CalFactory.java`

   该工厂用于生成具体场景下的计算器接口的某一个实现类。（本例中只有一个）

   ```java
   public class CalFactory{...}
   ```

4. 测试`Main.java`

   ```java
   public class Main{...}
   ```

   

## 三、服务框架

### 1.创建

使用`IDEA`创建一个`SpringBoot`项目。

### 2.编写`Controller`

编写一个`demoController`类来处理`Request`请求。使用`@RestController`注解。

```java
@RestController
public class demoController {...}
```

遵循`Restful`风格，将加减乘除分作了四个资源。

将上一题中的计算器核心类略作修改，在本项目中使用

### 3.测试

![3.1](https://gitlab.com/Qingxing80/ahu-2021-xuhai/-/raw/main/images/3.1.png)

![3.2](https://gitlab.com/Qingxing80/ahu-2021-xuhai/-/raw/main/images/3.2.png)

![3.3](https://gitlab.com/Qingxing80/ahu-2021-xuhai/-/raw/main/images/3.3.png)

![3.4](https://gitlab.com/Qingxing80/ahu-2021-xuhai/-/raw/main/images/3.4.png)

(在`@RestController`注解作用下，`demoController`内所有的方法都会返回`json`格式的数据，这样便于前端从接口中获取数据)



## 四、数据库

### 1.准备

#### (1)安装MySQL

#### (2)新建数据库

#### (3)新建表

```sql
use ahu_2021_test;
create table calc_record
(
	id int NOT NULL auto_increment,
    operand1 double  NOT NULL,
    oprator char(1) NOT NULL,
    operand2 double NOT NULL,
    result double NOT NULL,
    primary key(id)
);
```



### 2.在后端服务框架中增加数据库连接模块

#### (1)配置数据库连接文件`application.yml`

![4.1](https://gitlab.com/Qingxing80/ahu-2021-xuhai/-/raw/main/images/4.1.png)

#### (2)在项目中添加数据库写入语句

编写了一个辅助类`BuildSql`用于构造`SQL`语句

```java
public class BuildSql {
    String op;
    double a;
    double b;
    double res;
    public BuildSql(double a,String op,double b,double res){
        this.op = op;
        this.a = a;
        this.b = b;
        this.res = res;
    }
    public String build(){
        StringBuilder sb = new StringBuilder("insert into ");
        sb.append("ahu_2021_test.calc_record(operand1,oprator,operand2,result) ");
        sb.append("value(");
        sb.append(a+",");
        sb.append("\'"+op+"\'"+",");
        sb.append(b+",");
        sb.append(res);
        sb.append(");");
        return sb.toString();
    }
}
```

使用封装类`JdbcTemplate`来执行`SQL`语句

```java
JdbcTemplate jdbcTemplate;

jdbcTemplate.update(sql);
```



### 3.测试

![4.3](https://gitlab.com/Qingxing80/ahu-2021-xuhai/-/raw/main/images/4.3.png)

![4.4](https://gitlab.com/Qingxing80/ahu-2021-xuhai/-/raw/main/images/4.4.png)

![4.5](https://gitlab.com/Qingxing80/ahu-2021-xuhai/-/raw/main/images/4.5.png)

![4.6](https://gitlab.com/Qingxing80/ahu-2021-xuhai/-/raw/main/images/4.6.png)



## 五、容器

### 1.安装Docker

在阿里云服务器中安装`Docker`

### 2.在Docker中部署MySQL

#### (1)安装

```shell
# docekr pull mysql 
```

#### (2)启动

```shell
# docker run -d -p 3306:3306 -e MYSQL_ROOT_PASSWORD=root --name usemysql mysql
```

#### (3)创建数据库和表

### 3.将项目部署到Docker

#### (1)将`springboot`项目打包成`.jar`

由于将数据库部署到了服务器的Docker上，故需要修改项目中的数据库配置文件：将配置文件中的url修改为云服务器的实际IP地址即可。

使用Maven将项目打包成`.jar`:

![5.1](https://gitlab.com/Qingxing80/ahu-2021-xuhai/-/raw/main/images/5.1.png)

![5.2](https://gitlab.com/Qingxing80/ahu-2021-xuhai/-/raw/main/images/5.2.png)

#### (2)编写`Dockerfile`

(我将自动生成`jar`包的名字改为了`use_docker.jar`)

```
FROM openjdk:8-jre

RUN mkdir /app

COPY use_docker.jar  /app/

CMD java -jar  /app/use_docker.jar

EXPOSE 8080
```

#### (3)构建Docker镜像

将`use_docker.jar`和`Dockerfile`上传到云服务器。

构建项目的Docker镜像,命名为`testdocker`

```shell
# docker build -t testdocker .
```

![5.3](https://gitlab.com/Qingxing80/ahu-2021-xuhai/-/raw/main/images/5.3.png)

### 4.测试

#### (1)运行项目镜像：

```shell
# docker run -d -p 8080:8080  --name testdocker01 testdocker
```

![5.4](https://gitlab.com/Qingxing80/ahu-2021-xuhai/-/raw/main/images/5.4.png)

#### (2)查看正在运行的镜像，可见我们的项目镜像和`MySQL`数据库镜像正在运行：

```shell
# docker ps
```

![5.5](https://gitlab.com/Qingxing80/ahu-2021-xuhai/-/raw/main/images/5.5.png)

#### (3)测试

使用服务器的公网IP访问，可见服务部署成功：

![5.6](https://gitlab.com/Qingxing80/ahu-2021-xuhai/-/raw/main/images/5.6.png)

![5.7](https://gitlab.com/Qingxing80/ahu-2021-xuhai/-/raw/main/images/5.7.png)

使用`MySQL Workbench`查看部署在Docker上的数据库，可见数据库也连接成功：

![5.8](https://gitlab.com/Qingxing80/ahu-2021-xuhai/-/raw/main/images/5.8.png)

